data "aws_vpc" "yogi-vpc"{
filter {
 name = "tag:Name"
 values = ["Yogi-VPC-DevOps"]
}
}

data aws_subnets "public-subnets" {
filter {
   name   = "vpc-id"
   values = [data.aws_vpc.yogi-vpc.id]
  }
  tags = {
   Name = "public-subnet-*"
  }
}

data "aws_iam_role" "example" {
  name = "eks-node-group-example"
}

data "aws_eks_cluster" "eks_creation" {
  name = var.eks-cluster-name1 
}

data "aws_subnet" "public-subnets" {

count = "${length(var.public-subnet-cidr)}"
id = "${tolist(data.aws_subnets.public-subnets.ids)[count.index]}"
}


resource "aws_security_group" "node_group_sg" {
 vpc_id            = data.aws_vpc.yogi-vpc.id
   ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
	}
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
	}
}

resource "aws_launch_template" "eks_launch_template" {
  name          = "test"

  image_id      = "ami-07f0f3deaa0c4dffa"
  update_default_version = false  
  key_name = "jenkins"
  lifecycle {
    create_before_destroy = true
  }
   vpc_security_group_ids = [aws_security_group.node_group_sg.id]
}

resource "aws_eks_node_group" "worker-node-group-app" {
 count = "${length(var.public-subnet-cidr)}"
  cluster_name  = data.aws_eks_cluster.eks_creation.name
  node_group_name = "sandbox-workernodes-app"
  node_role_arn  = data.aws_iam_role.example.arn
  subnet_ids = flatten([data.aws_subnet.public-subnets[*].id])
  instance_types = ["t2.medium"]

  scaling_config {
   desired_size = 1
   max_size   = 2
   min_size   = 1
  }
labels = {
  name = "application"
}
 
taint {
  key    = "wp-k8s"
  value  = "application"
  effect = "NO_SCHEDULE"
}

 }

resource "aws_eks_node_group" "worker-node-group-db" {
 count = "${length(var.public-subnet-cidr)}"
  cluster_name  = data.aws_eks_cluster.eks_creation.name
  node_group_name = "sandbox-workernodes-db"
  node_role_arn  = data.aws_iam_role.example.arn
  subnet_ids = flatten([data.aws_subnet.public-subnets[*].id])
  instance_types = ["t2.medium"]
  scaling_config {
   desired_size = 1
   max_size   = 2
   min_size   = 1
  }

labels = {
  name = "database"
}

taint  {
  key    = "wp-k8s"
  value  = "database"
  effect = "NO_SCHEDULE"
}

 }


  
 